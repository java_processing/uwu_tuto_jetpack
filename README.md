
# TUwUto : programmation d'un jeu en java/processing


## Avant de commencer

L'**objectif** de ce tuto, c'est de t'apprendre quelques bases en **java** (et plus généralement en programmation **orientée objet** (POO en francais, OOP en anglais). Mais aussi faire un petit jeu plutôt cool, accessible et pas trop ch***. Dans ce petit jeu en 2D très simple, on va contrôler un personnage avec un jetpack :rocket:. Le but va être de ramasser un maximum d':star: tout en naviguant entre les obstacles ; et en esquivant ceux qui font des dégats.

<br>

**Disclaimer 1** : Je suis pas un pro de la programmation, j'ai quasiment tout appris sur internet en suivant des tuto, en allant lire les doc et en testant des choses. Il est donc très probable que ce tuto contienne des **erreurs** (que ce soit des erreurs de vocabulaire, des concepts que j'ai mal compris/expliqués ou des "pratiques" qui ne se font pas trop dans le milieu de la programmation). 
Si vous pensez que j'en ai fait une, n'hésitez pas à me le dire.

**Disclaimer 2** : java-**processing** "cache" pas mal de lignes de code en arrière plan afin que l'utilisateur (toi et moi en gros) puisse programmer ses animations le plus facilement et **ergonomiquement** possible.
Par exemple, en java, tout programme contient au moins une **classe** (on verra lors de ce tuto ce que c'est) et commence par une fonction appelée **main** écrite à l'intérieur de cette même classe (La fonction main est la fonction "porte d'entrée" du programme pour faire simple). 
Tu verras pourtant qu'on n'aura pas besoin d'écrire cette fonction car elle est déjà écrite pour nous (cachée).
Il n'y a pas que la fonction main qu'on aura pas besoin d'écrire, d'autres d'outils ont été codés en arrière plan pour nous faciliter la tâche.
Sans plus rentrer dans les détails pour le moment, il faut donc être au courant que certaines choses possibles en java "normal" ne sont pas possibles dans l'environnement de programmation java-processing étant donné la façon dont il est construit.
J'en reparlerais sûrement plus tard dans ce tuto.

<br>

**Prérequis** :   
* Avoir installé java **processing** sur son PC
* Savoir comment lancer un **sketch** et comment ouvrir un nouvel **onglet**
* Savoir utiliser les structures telles que :
	* if else
	* for
	* while et do while
	* etc..
* Savoir comment **affecter** une valeur à une **variable**
* Notions de base en **géométrie** et en **mécanique**

<br>

**Pour te faciliter la lecture de ce tuto** :   
>:bulb:   
>Ce tutoriel est plutot **long** car il rentre beaucoup dans les **détails**. Ces détails/précisions/aides seront écrits dans ces blocs gris avec une **ampoule** :bulb:.
>Du coup, si tu ne veux pas passer trop de temps sur ce tuto, tu peux passer ces commentaires, mais tu risques de ne pas forcément tout comprendre.
>Et si tu te sens vraiment chaud en programmation et que tu connais processing, la lecture des titres et du code devrait suffire pour comprendre.

<br>

# Les bases   

## Le fond de la fenêtre

Comme je te l'ai dit dans le *disclaimer*, il n'y a pas besoin d'écrire de fonction main pour exécuter un sketch. Pour en exécuter un, t'as juste à écrire le code en dessous :  
```java
void setup() {
  size(800, 600);  // taille de la fenêtre en pixels (en pixels)
}

void draw() {
  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
}
```
Dans le code ci-dessus, on a **implémenté** les fonctions **setup** et **draw**.
La fonction **setup()** sert à initialiser l'animation.  
La fonction **draw()** s'execute pour chaque frame (par defaut, et si la puissance de calcul le permet, le programme tourne à 60fps).  

Dans ces fonctions, on a utilisé d'autres fonctions **déjà écrites** pour nous.
Par exemple, on a utilisé la fonction **size(800, 600)** qui créé une fenêtre de la taille souhaitée.

<br>

>:bulb:   
>Pour les **débutants** (ou petit rappel pour ceux qui auraient oublié) :  
>* "*C'est quoi **void** ??*" --> pour faire simple, ça veut dire que notre fonction ne retourne pas de valeur, elle ne fait qu'exécuter une série d'**instructions**.   
>	* Si on veut une fonction qui retourne un entier (par exemple), il faut remplacer "**void**" par "**int**" pour spécifier le **type de donnée** que la fonction retourne (et il faut ajouter une instruction qui retourne effectivement un entier).
>	* Si on veut retourner la variable i contenant un entier, on ajoute l'instruction "return i;" dans la fonction.  
>* Une **instruction** se finit par un point virgule.  
>* Le **corps** d'une fonction se trouve entre accolades et est composé d'une ou plusieurs instructions.
>* Si la fonction que l'on code a besoin de **paramètres**, il faut spécifier leur type et nom dans les  parenthèses de la fonction. 
> Une fonction qui se trouve à l'intérieur d'une classe s'appelle une **méthode**.

<br>

Le bloc qui suit n'est **pas nécessaire** si tu veux juste t’amuser un peu sur processing.
Cependant, il explique dans les grandes lignes les **bases du fonctionnement** d'un programme **java**, il peut donc être très utile si tu veux en savoir plus.
>:bulb:
>Pour ceux qui veulent aller **plus loin** :
>Quand on exécute ce code, le vrai programme **java** exécuté derrière est :
>```java
>public class TUwUto_jetpack extends PApplet {
>
>  static public void main(String[] passedArgs) {
>    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "TUwUto_jetpack" };
>    if (passedArgs != null) {
>      PApplet.main(concat(appletArgs, passedArgs));
>    } else {
>      PApplet.main(appletArgs);
>    }
>  }
>  
>  void setup() {
>    size(800, 600);  // taille de la fenêtre en pixels (en pixels)
>  }
>
>  void draw() {
>    background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
>  }
>}
>```
>Comme déjà dit dans le **Disclaimer 2** : "*Tout programme contient au moins une classe et commence par une fonction appelée **main** écrite à l'intérieur de cette même classe*". 
>On peut voir ici cette fameuse méthode main (qui ne fait pas grand chose d'autre qu'éxécuter une autre méthode main) qui se trouve à l'intérieur de la classe "***TUwUto_jetpack***" (c'est le nom que j'ai donné au sketch).
>> Si tu ne comprends pas le code de la fonction *main*, ni ce que veut dire "*extends*", ni ce qu'est "*PApplet*", ce n'est pas grave. Ce n'est pas important pour le moment
>
>On remarque que cette classe contient aussi nos 2 fonctions **setup()** et **draw()**, qui sont en fait des méthodes.
>En fait, tout ce qu'on code dans un **sketch processing** se retrouve **copié-collé** à l'intérieur de cette classe.
>Cela peut être **contraignant** si on veut écrire du code en dehors de cette dernière. Si je poursuis ce tuto assez loin, on verra comment faire pour contourner ce problème (qui pour l'instant n'en est pas un).
 

## Affichage d'une forme / Système d'axe

On veut maintenant afficher un personnage dans notre fenêtre (en fait ça va être un rond ; mais tqt, avec un peu d'imagination ça le fait :sweat_smile:).   
Pour se faire, t'as juste à ajouter la ligne suivante **en dessous** de l'instruction "background(41);" :  
```java
ellipse(0f, 0f, 20f, 20f);  // affiche un disque de diamètre 20 à l'origine. 
```

<br>

>:bulb:   
>**Pour ceux qui se demandent** "*Pourquoi il met des* **f** *de partout celui-là ??*"   
>La réponse est : Si on ne met pas de f, il y a ambiguïté sur le **type de donnée** (ça peut être soit un **int** (entier), soit un **float** (réel)).   
>Dans certains cas, ne pas mettre de points ne pose pas de problème (comme ici), mais parfois, le comportement peut être différent avec ou sans point (pour la division par exemple).   
>C'est donc mieux de toujours préciser quel type de donnée on veut. Ici on veut des float car la fonction ellipse prends des float en entrée.    

<br>

Comme t'as pu le remarquer en lançant le programme, l'**origine** se trouve en haut à gauche de la fenêtre (et le point (800, 600) se trouve en bas à droite).   
Ça serait plus simple si l'origine était au milieu et que l'axe vertical soit orienté de bas en haut ; nan ? 
<br>

C'est tout simple à faire, t'as juste à rajouter ces 2 lignes dans la fonction draw().   
```java
translate(width/2, height/2);  // place l'origine au milieu de l'écran
scale(1, -1);  // oriente l'axe vertical vers le haut
```

<br>

Si tout fonctionne bien, tu devrais avoir un disque blanc au milieu de ta fenêtre.  
Si c'est pas le cas (et que l'ellipse se trouve toujours en haut à gauche), c'est s**UwU**rement que t'as écrit ces 2 lignes après avoir dessiné l'ellipse.   
Dessines l'ellipse après les 2 lignes de code que tu viens de rajouter et ça devrait marcher.   

>:bulb:   
>**Pour ceux qui se demandent** "*Elles viennent d'où ces 2 variables **width** et **height** ? Je les ai même pas déclarées !*"   
La réponse est : elles font parti des éléments qui sont codés en **arrière plan** (ce sont des variables qui font parties de la classe *PApplet*). 
Il y a d'autres variables du même genre, on peut notamment citer **frameRate** ou **frameCount** (parmi d'autres).    

<br>

A ce moment du tUwUto, ton code devrait ressembler à ça :  
```java
void setup() {
  size(800, 600);  // taille de la fenêtre (en pixels)
}
void draw() {
  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
  translate(width/2, height/2);
  scale(1, -1);  
  ellipse(0f, 0f, 20f, 20f);  // disque de diamètre 20 à l'origine
}
```
Par la suite, ça sera plus simple de réfléchir avec le rayon au lieu du diamètre.
Ajoute ça dans ta fonction **setup** (et change *20f* par *10f*):
```java
ellipseMode(RADIUS);
```

<br>

# Un peu de mouvement ?  
## Gravité  

C'est bien joli tout ça mais pour l'instant c'est plutôt ennuyant, ya rien qui bouge..  
On va commencer par faire ressentir de la **gravité** à notre personnage.  
Pour cela on va avoir besoin d'un **vecteur** gravité, d'un vecteur vitesse, et d'un vecteur position.  
On va maintenant **déclarer** et **initialiser** (en même temps) nos vecteurs (en dehors des fonctions setup() et draw()).   
```java
PVector g = new PVector(0f, -0.8f);  // la vitesse augmente de 0.8 vers le bas à chaque frame (à 60fps)
PVector vitesse = new PVector(0f, 0f);  // vitesse initiale nulle
PVector position = new PVector(0f, 0f);  // apparait au centre de l'écran
```

<br>

Ce sont bien des **objets** que tu as devant toi. Plus précisément, ce sont des **instances** de la **classe** PVector.   
Jusqu'alors, on avait juste parlé de float ou de int qui sont des types dits "**primitifs**" (il y en a d'autres).   
Pour instancier une classe, on utilise un des **constructeur** de ladite classe.   

(Si t'as pas trop compris tout ce jargon d'informaticien c'est pas bien grave, tu peux continuer le tuto. Mais je t'invite quand même à lire le bloc ci-dessous :point_down:, ça fait pas de mal).  

>:bulb:   
>**Complément d'information**
>Pour déclarer une variable de type **primitif** ou déclarer un **objet**, c'est assez semblable :  
>```java
>float monFloat;	 // déclaration d'un float (type primitif)
>PVector monVecteur;  // déclaration d'un objet de la classe PVector (par exemple)
>```
>Par contre, pour les initialiser, c'est pas tout à fait pareil :  
>```java
>monFloat = 12.34f;  // initialisation d'un float
>monVecteur = new PVector(0f, 0f);  // instanciation d'un objet de la classe PVector
>```
>Pour une variable de type primitif, il y a juste besoin d'écrire sa valeur.   
>Mais pour **instancier** cette classe (cad créer un objet qui est un exemplaire de cette classe), il faut faire appel à un des constructeurs de cette classe (car une classe peut en avoir plusieurs).   
En l’occurrence, étant donné qu'on est en 2D, le constructeur demande 2 coordonnées (si on était en 3D, on aurait utilisé le constructeur qui prends 3 coordonnées).   
>>**Remarque**: par convention, le nom d'une variable commence toujours par une lettre minuscule

---

<br>

Pour faire bouger notre personnage, on met à jour la vitesse avec l'accélération, puis on met à jour la position avec la vitesse
(en considérant que notre framerate est constant, on a un intervalle de temps **dt** constant).   
Ça nous donne le code suivant (à écrire à la place de la ligne qui affiche l'ellipse):   
```java
vitesse.add(g);
position.add(vitesse);  
ellipse(position.x, position.y, 10f, 10f);
```

<br>

Comme t'as pu le remarquer, pour **additionner** nos PVectors ensemble, on a pas utilisé l'opérateur **+** comme on l'aurait fait pour des float ou des int.   
A la place on a utilisé la (une des) fonction **add** de la classe PVector; qui prends un objet PVector en paramètre et qui l'additionne au vecteur sur lequel on applique la fonction (les vecteurs vitesse et position sont donc modifiés).

>:bulb:   
>**Complément d'information**
>La classe PVector a d'autres fonctions. On peut citer par exemple la fonction "**dot**" qui calcule le produit scalaire, ou la fonction "**cross**" qui calcul le produit vectoriel.   
>T'auras plus d'info en regardant directement la [référence de la classe PVector](https://processing.org/reference/PVector.html).   

>:bulb:   
>**Complément d'information**
> La fonction **add** a aussi une version **statique**.
> Je vais t'expliquer ce que c'est en utilisant un exemple.
> Si on veut additionner les vecteurs position et vitesse; mais qu'on veut "ranger" le résultat de l'addition dans une nouvelle variable (et laisser intact les vecteurs position et vitesse).   
> On doit utiliser la version statique de la fonction add, qui retourne une nouvelle instance de la classe PVector que l'on peut ranger dans une nouvelle variable:  
>```java
>PVector monResultat = PVector.add(vitesse, g);
>```
>La fonction statique s'utilise sur la classe PVector en elle même, et non sur une instance de la classe PVector (en vrai, c'est possible;  mais ça n'a aucun sens de le faire).   
>Par contre, utiliser une fonction non statique sur la classe PVector est impossible car la classe PVector est (comme son nom l'indique) la classe PVector, et non une **instance** de la classe PVector:  
>```java
>PVector monResultat = PVector.add(g);  // impossible !!!
>```
>Le fait d'avoir plusieurs méthodes portant le même nom s'appelle de la **surcharge de méthode**. Je reparlerais de surcharge plus loin dans le tuto.

<br>

## Vers l'infini et l'au dela !!  

### Ouais, mais pas trop loin  

Notre petit personnage **tombe**.. Et il continue de tomber pour toujours et se perds..:cry:   
Et nous on veut pas de ça. On veut que quand il parte; il revienne.   
Et pour ça, on a qu'à le faire revenir par le **côté opposé** où il disparaît.  
Pour se faire, on doit rectifier la position si elle sort des limites de la fenêtre.   
Ce qui me paraît le plus facile à implémenter, c'est de rajouter ces lignes de code juste **après le calcul de la position**.
```java
if(position.y < -height / 2.) {position.y += height;}
if(position.y > height / 2.) {position.y -= height;}
if(position.x < -width / 2.) {position.x += width;}
if(position.x > width / 2.) {position.x -= width;}
```
> On pourrait aussi utiliser l'opérateur **modulo**.

<br>

### Et pas trop vite non plus  

Normalement, si t'as bien tout codé, tu devrais maintenant avoir un personnage qui **accélère indéfiniment**.   
Et ça, on en veut pas non plus. Ça va être trop dur à **contrôler**..:dizzy_face:    
On pourrait simplement définir une vitesse maximale. Mais c'est pas très réaliste.
On va plutot implémenter une force de **traînée** :

![Force de trainee](https://wikimedia.org/api/rest_v1/media/math/render/svg/14b14964ce3902ca46c70ee77e10b3ca3b0751a1)	<sup>(Plus d'info sur [la page wikipedia](https://fr.wikipedia.org/wiki/Tra%C3%AEn%C3%A9e))</sup>

 Pour simplifier tout ça, on va considérer :
* *Cx = 2*
* *S = r²*

Afin d'avoir *F<sub>x</sub>=$\rho$r²V²*

Je te laisse donc :
* créer la variable r (de type float) qui contient le rayon de l'ellipse
* créer la variable rho ($\rho$) (float) et l'initialiser à la valeur 6e-6f (pareil que 0.000006f) que j'ai trouvé par tatonnement.

T'as plus qu'à écrire le code suivant (juste après la ligne *vitesse.add(g)*):
```java
  float vMag = vitesse.mag();
  PVector trainee = vitesse.copy().normalize().mult(rho * r * r * vMag * vMag).limit(vMag);
  vitesse.sub(trainee);
```
>:bulb:
> Pour ceux qui n'ont **pas compris** :
>* La méthode **mag()** retourne la norme du vecteur (ici le vecteur vitesse)
>* La méthode **copy()** retourne une copie du vecteur vitesse (afin que le vecteur vitesse original ne soit pas affecté par la fonction qui suit)
>* La méthode **normalize()** normalise le vecteur à une longueur de 1
>* La méthode **mult(a)** multiplie le vecteur par la valeur *a* (qui est ici la valeur absolue de notre force de traînée)
>* La méthode **sub()** fait la soustraction
>* La méthode **limit(a)** limite la norme du vecteur à la valeur de *a*. Ici la limite est vMag pour ne pas que notre personnage change carrément de direction 
>> C'est une façon de formaliser le fait que la force de traînée est **non conservative**. D'ailleurs, je n'ai pas introduit de masse dans mon code, ce qui rend équivalent les concepts de force et d'accélération.
>
> La variable traînée n'est pas définie en **dehors** de la fonction **draw** car elle ne sert qu'a l'intérieur de cette dernière.

<br>

## PLEIN GAAAAAAZ !!!!... Enfin, pas trop... C'est pas très troisième E tout ça..:earth_africa:   

### Détection des touches   

Il est enfin temps de fournir un jetpack :rocket: à notre personnage afin qu'il puisse se diriger.
On va devoir implémenter des **contrôles** pour notre petit jetpack.    
Pour moi, je vais tout simplement utiliser les **flèches directionnelles**.   
Encore une fois, on va utiliser les fonctionnalités mises à notre disposition par processing.   
Ajoute à ton code la fonction suivante, puis appuis sur les flèches directionnelles:   
```java
void keyPressed() {
  println(keyCode);
}
```

<br>

La fonction **keyPressed()** est une fonction qui est appelée dès qu'on appuis sur une touche du clavier (si on appuie sur 2 touches en même temps, elle s'exécute 2 fois (sans ordre prédéfini)).   
L'instruction "println(**keyCode**);" affiche dans la **console** le code correspondant à la touche sur laquelle on appuie.   
Pour ma part, j'ai :  
* :arrow_left: : 37
* :arrow_up: : 38
* :arrow_right: : 39
* :arrow_down: : 40

Maintenant qu'on connait le code de nos touches, on peut **remplacer le précédent** code par celui-là :   
```java
boolean gauche = false;
boolean haut = false;
boolean droite = false;
boolean bas = false;

void keyPressed() {
  commande(keyCode, true);
}

void keyReleased() {
  commande(keyCode, false);
}

void commande(int code, boolean activation) {
  switch (code) {
    case 37:
      gauche = activation;
      break;
    case 38:
      haut = activation;
      break;
    case 39:
      droite = activation;
      break;
    case 40:
      bas = activation;
      break;
  }
}
```

<br>

Rien de compliqué ici : si on appuie sur la flèche du haut (fonction keyPressed() avec keyCode = 38), 
--> alors la variable **booléenne** "haut" devient **vraie**.   

Mais dès lors qu'on relâche cette touche (fonction keyReleased()),
--> alors la variable "haut" devient **fausse**.   
Même fonctionnement pour les touches gauche, droite et bas.  

>:bulb:   
>Pour les **débutants** (ou petit rappel pour ceux qui auraient oublié) :  
>* Une variable **booléenne** est une variable qui ne peut prendre que 2 valeurs : **true** ou **false**  
>* La structure "**switch case**" prends en paramètre un entier (ici la variable **keyCode**).  
>Si ce paramètre est égal à un des entiers (37, 38, 39 ou 40 dans notre cas), alors la/les instructions correspondant à l'entier égal au paramètre sont exécutées.   

>:bulb:
>Les variables booléennes sont initialisées à **false** car, par défaut, le jetpack n'est pas activé.

<br>

### Contrôler les déplacements  

La **poussée verticale** ne se fait que si la variable "haut" est vrai.  
Pour effectuer cette poussée, il faut :  
* Créer un nouveau vecteur qu'on va appeler "**accHaut**" avec une accélération vers le haut de 2f.
* Puis, au même endroit où mets à jour la vitesse avec l'accélération de la pesanteur :  
	* Si haut est vrai : on mets à jour la vitesse en lui additionnant pousseeHaut  
	* Sinon, on ne fait rien  

Ça se traduit par le code suivant :  
```java
if (haut) {	// si haut est vrai
  vitesse.add(accHaut);  // si la variable haut est vraie, on active la poussée vers le haut
}
```

<br>

A partir de maintenant, tu devrais être capable de contrôler l'**altitude** de ton jetpack.  

>:bulb:   
>Need **help** ?
>Pour que processing prenne en compte les touches sur lesquelles on appuie, il faut avoir sélectionné la fenêtre en cliquant dessus.  

<br>

Il ne te reste plus qu'à faire pareil pour les autres touches directionnelles (je te laisse choisir quelle valeur d'accélération tu veux pour la poussée latérale :wink:).
Tu devrais avoir un code qui ressemble à ça :
```java

PVector accHaut = new PVector(0f, 2f);
PVector accBas = new PVector(0f, -2f);
PVector accDroite = new PVector(0.85f, 0f);
PVector accGauche = new PVector(-0.85f, 0f);

PVector g = new PVector(0f, -0.8f);  // la vitesse augmente de 0.8 vers le bas à chaque frame (à 60fps)
PVector vitesse = new PVector(0f, 0f);  // vitesse initiale nulle
PVector position = new PVector(0f, 0f);  // apparait au centre de l'écran

float r = 10f;
float rho = 6e-6f;

void setup() {
  size(800, 600);  // taille de la fenêtre (en pixels)
  
  ellipseMode(RADIUS);
}

void draw() {
  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
  
  translate(width/2, height/2);
  scale(1, -1);
  
  vitesse.add(g);
  
  if (haut) {vitesse.add(accHaut);}
  if (bas) {vitesse.add(accBas);}
  if (gauche) {vitesse.add(accGauche);}
  if (droite) {vitesse.add(accDroite);}
  
  float vMag = vitesse.mag();
  PVector trainee = vitesse.copy().normalize().mult(rho * r * r * vMag * vMag).limit(vMag);
  vitesse.sub(trainee);
  position.add(vitesse);
  ellipse(position.x, position.y, r, r);
  
  if(position.y < -height / 2.) {position.y += height;}
  if(position.y > height / 2.) {position.y -= height;}
  if(position.x < -width / 2.) {position.x += width;}
  if(position.x > width / 2.) {position.x -= width;}
}

boolean gauche = false;
boolean haut = false;
boolean droite = false;
boolean bas = false;

void keyPressed() {
  commande(keyCode, true);
}

void keyReleased() {
  commande(keyCode, false);
}

void commande(int code, boolean activation) {
  switch (code) {
    case 37:
      gauche = activation;
      break;
    case 38:
      haut = activation;
      break;
    case 39:
      droite = activation;
      break;
    case 40:
      bas = activation;
      break;
  }
}
```

<br>

# La programmation orientée objet

## Entrée en matière

Dans le code ci-dessus :point_up: , on n'a utilisé qu'un seul **objet** - l'objet PVector - et cet objet, c'est même pas nous qui l'avons codé.
Autrement dit, avec ce que je t'ai fais coder jusqu'à maintenant, on est loin de la **programmation orientée objet**.
Pour bien te faire comprendre l'intéret de la **POO**, je vais te préciser un peu ce qu'on veut faire pour notre jeu.

On veut avoir un personnage qui : 
* Possède un **score** (nombre d':star: ramassées)
* Possède 2 :heart: de **vie** (certains obstacles pourront causer des dommages)
* Utilise un **jetpack** pour se diriger (et qu'il puisse **commander**)
* A une **position** et une **vitesse** (il subit des accélérations en fonction de l'environnement et de son jetpack)

<br>

La **POO** nous permet non seulement de créer ces **objets** (au lieu d'avoir à entasser des variables comme on l'a fait jusqu'à présent), mais aussi d'en définir le/les **comportements** en codant des fonctions de classe, autrement appelées méthodes (comme la méthode add de la classe PVector par exemple).

Du coup, on va devoir faire quelques modifications à notre code. Mais tqt pas, ça va bien se passer :wink: .

>:bulb:
>Normalement, dans les parties qui suivent, tu vas voir apparaître des erreurs dans ton code.
>C'est normal, ça ne va durer que le temps de faire nos petites modifications :smiley:

## Un jetpack qui a de la classe ! :rocket: :sunglasses: (petit jeu de mot t'as vu)

### 1. Créer la classe
Dans cette partie, on va coder la **classe Jetpack** (c'est la plus simple à coder).
Pour cela, il faut ouvrir un nouvel **onglet** qu'on va appeller **Jetpack** (c'est pas obligé mais ça permet de mieux s'y retrouver).
Par convention, le nom d'une classe commence par une **majuscule**.
```java
class Jetpack {
}
```

<br>

### 2. Créer les variables

Comme dit précedemment, on veut que notre jetpack ait une accélération pour chaque direction. Il faut donc ajouter ces variables dans notre classe (et les enlever de notre programme original).

>:bulb:
>On appelle ces variables des **variables d'instance**, car elles sont propres à chaque instance de la classe Jetpack
>Si on fait précéder une variable par le mot clé **static** alors ce n'est plus une variable d'instance mais une **variable de classe** car elle sera la même pour toute instance de la classe.

Voici le code :
```java
class Jetpack {

  PVector accHaut;
  PVector accBas;
  PVector accDroite;
  PVector accGauche;
}
```
Ici, on ne fait que les **déclarer** et non les initialiser.
Tu vas très vite comprendre pourquoi en regardant ce qui suit :point_down:. 

<br>

### 3. Le constructeur

Il est temps d'implémenter le **constructeur** de la classe Jetpack. 
Le constructeur va contenir le code qui permet d'**initialiser l'objet** lorsqu'il est en train d'être instancié en utilisant (entre autres) les paramètres d'entrée.

```java
class Jetpack {
  
  PVector accHaut;
  PVector accBas;
  PVector accDroite;
  PVector accGauche;
  
  Jetpack(PVector aH, PVector aB, PVector aG, PVector aD) {
    this.accHaut = aH;
    this.accBas = aB;
    this.accGauche = aG;
    this.accDroite = aD;
  }
}
```
Ici, le constructeur initialise les variables d'instance (qu'on vient de déclarer dans la partie précédente) en itilisant les variable passées en paramètre d'entrée.

>:bulb:
>Le mot-clé **this** désigne, dans une classe, l'instance courante de la classe elle-même.
>Il a plusieurs utilisations. Par exemple, la cas ci dessus :point_up:, il n'est pas nécessaire et est simplement utilisé pour rendre le code plus **explicite**.
>(Tu trouveras plus d'info [ici](https://fr.wikibooks.org/wiki/Programmation_Java/this))

Une classe peut avoir **plusieurs constructeurs**, on appelle ça de la **surcharge** de constructeur (constructor **overloading** en anglais).

>:bulb:
>Le constructeur que j'ai fais ici est un peu lourd, mais il permet d'avoir un contrôle total sur la façon de diriger le jetpack. En effet, en demandant 4 PVector, on peut imaginer des mécaniques de gameplay comme l'introduction de défauts dans les "propulseurs" du jetpack.
>Si on veut se permettre cette grande liberté tout en ayant des constructeurs plus pratiques, on a qu'à en rajouter.

Je vais rajouter 2 constructeurs :
* Un qui demande 4 float
* Un qui demande 2 float

>:bulb:
>Pour que la surcharge soit possible, il faut que les paramètres soient de types différents.
>C'est impossible de faire 2 constructeurs qui demandent 4 float par exemple

* Pour **4 float** :
```java
Jetpack(float h, float b, float g, float d) {
  this(new PVector(0f, h),
       new PVector(0f, b),
       new PVector(g, 0f),
       new PVector(d, 0f));
}
```

Ce que j'ai fais ici, c'est appeler le constructeur qui demande 4 PVector à l'interieur du constructeur qui demande 4 float grâce à l'utilisation du mot-clé "**this**".
Ca peut être très utile quand on veut **réutiliser un constructeur** qui contient beaucoup de lignes de code mais qu'on ne veut pas tout réécrire.
Et surtout, si on veut modifier du code, on ne doit le faire qu'à un seul endroit au lieu de 2.

* Constructeur pour **2 float** :
```java
Jetpack(float vertical, float lateral) {
  this(vertical, -vertical, -lateral, lateral);
}
```

<br>

### 4. Instanciation et utilisation

Dans l'onglet principal :
Créé une instance de l'objet jetpack en utilisant le constructeur de ton choix.
Pour ma part, ça sera comme ça :
```java
Jetpack jet = new Jetpack(2f, 0.85f);
```

<br>

Puis, dans la fonction **draw()**, on a juste à faire une toute petite modification :
```java
if (haut) {vitesse.add(jet.accHaut);}
if (bas) {vitesse.add(jet.accBas);}
if (gauche) {vitesse.add(jet.accGauche);}
if (droite) {vitesse.add(jet.accDroite);}
```
Il suffit d'écrire "**jet.accHaut**" pour accéder à la variable d'instance *accHaut* de l'instance *jet*.

### 5. A ton tour

Ouvre un nouvel onglet et **implémente** une classe **Environnement** qui contient sa masse volumique $\rho$ (*rho*) ainsi que l'accélération de la pesanteur $g$.
Crée une instance de la classe puis utilise là dans le code à la place des anciennes variables g et rho.

Ton code devrait ressembler à quelque chose comme ça :
>* Onglet principal *TUwUto_jetpack* :
>```java
>
>Jetpack jet = new Jetpack(2f, 0.85f);
>Environnement envrnt = new Environnement(new PVector(0f, -0.8f), 6e-6f);
>
>PVector vitesse = new PVector(0f, 0f);  // vitesse initiale nulle
>PVector position = new PVector(0f, 0f);  // apparait au centre de l'écran
>
>float r;
>
>void setup() {
>  size(800, 600);  // taille de la fenêtre (en pixels)
>
>  ellipseMode(RADIUS);
>}
>
>void draw() {
>  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = >blanc)
>  
>  translate(width/2, height/2);
>  scale(1, -1);
>
>  vitesse.add(envrnt.g);
>  
>  if (haut) {vitesse.add(jet.accHaut);}
>  if (bas) {vitesse.add(jet.accBas);}
>  if (gauche) {vitesse.add(jet.accGauche);}
>  if (droite) {vitesse.add(jet.accDroite);}
>  
>  float vMag = vitesse.mag();
>  PVector trainee = vitesse.copy().normalize().mult(envrnt.rho * r * r * vMag * vMag).limit(vMag);
>  vitesse.sub(trainee);
>  position.add(vitesse);
>  ellipse(position.x, position.y, r, r);
>  
>  if(position.y < -height / 2.) {position.y += height;}
>  if(position.y > height / 2.) {position.y -= height;}
>  if(position.x < -width / 2.) {position.x += width;}
>  if(position.x > width / 2.) {position.x -= width;}
>}
>
>boolean gauche = false;
>boolean haut = false;
>boolean droite = false;
>boolean bas = false;
>
>void keyPressed() {
>  commande(keyCode, true);
>}
>
>void keyReleased() {
>  commande(keyCode, false);
>}
>
>void commande(int code, boolean activation) {
>  switch (code) {
>    case 37:
>      gauche = activation;
>      break;
>    case 38:
>      haut = activation;
>      break;
>    case 39:
>      droite = activation;
>      break;
>    case 40:
>      bas = activation;
>      break;
>  }
>}
>```
>* Onglet *Environnement* :
>```java
>class Environnement {
>
>  PVector g;
>  float rho;
>  
>  Environnement(PVector g, float rho) {
>    this.g = g;
>    this.rho = rho;
>  }
>}
>```
>* Onglet *Jetpack* :
>```java
>class Jetpack {
>  
>  PVector accHaut;
>  PVector accBas;
>  PVector accDroite;
>  PVector accGauche;
>  
>  Jetpack(PVector aH, PVector aB, PVector aG, PVector aD) {
>    this.accHaut = aH;
>    this.accBas = aB;
>    this.accGauche = aG;
>    this.accDroite = aD;
>  }
>  
>  Jetpack(float h, float b, float g, float d) {
>    this(new PVector(0f, h),
>         new PVector(0f, b),
>         new PVector(g, 0f),
>         new PVector(d, 0f));
>  }
>  
>  Jetpack(float vertical, float lateral) {
>    this(vertical, -vertical, -lateral, lateral);
>  }
>}
>```

<br>

## Un personnage héritié (tu vas comprendre :smirk:)

### 1. L'héritage

Notre personnage, il a une position; mais pas que. 
Il a aussi une vitesse, il porte un jetpack (et on peut lui imaginer toute une panoplie d'autres équipements pour notre jeu).
Mais si on veut l'afficher, il faut avant tout qu'il ait une **position**. 
Donc, pour nous simplifier la vie, on n'a qu'à dire que notre **personnage**, c'est un **vecteur** auquel on ajoute d'autres informations, nan ? (de toute façon t'as pas le choix :smiling_imp: )

Je te montres comment on fait ça puis je t'explique (penses bien à ouvrir un nouvel **onglet**) :
```java
class Personnage extends PVector{
}
```

Ce qu'on vient de faire là :point_up:, ça s'appelle de l'**héritage** de classe (**inheritance**).
Petit point vocabulaire, on dit que :
* Personnage **hérite** de PVector
* Personnage est une **sous-classe** de PVector (**subclass**)
* PVcetor est la **classe parente** de Personnage (**superclass**).

Pour faire simple, cela veut dire que notre classe a toutes les **caractéristiques** d'un PVector (en plus de celles qu'on va lui donner) et peut utiliser toutes les **fonctions** d'un PVector (en plus de celles qu'on va coder).

>:bulb:
>Il est possible de faire de l'**héritage multiple** (c'est à dire qu'il est possible de créer une classe qui hérite de la classe Personnage, et ainsi de suite).

<br>

### 2. Les variables

On va maintenant déclarer les variables d'instance de notre Personnage :
```java
class Personnage extends PVector {

  float r;
  
  PVector v;  //vitesse
  Jetpack jet;
  
  boolean gauche = false;
  boolean haut = false;
  boolean droite = false;
  boolean bas = false;
}
```
Etant donné que c'est notre personnage qui contrôle le jetpack, j'ai décidé de mettre les variables *booléennes* dans la classe Personnage (on peut donc enlever ces variables de l'onglet principal (on peut aussi enlever les variables *vitesse*, *position* et *r*)).

>:bulb:
>**Complément d'information**
>On ne déclare pas de vecteur **position** dans notre classe Personnage car cette dernière hérite de la classe PVector. En effet, les coordonnées x et y sont écrites dans la classe PVector (la classe parente de Personnage).

<br>

### 3. Les constructeurs

Il nous faut ensuite un constructeur (que je vais surcharger).
Je te montre tout ça en-dessous :point_down:, puis je t'explique :
```java
Personnage(float x, float y, float vX, float vY, Jetpack j, float r) {
  super(x, y);
  this.v= new PVector(vX, vY);
  this.jet = j;
  this.r = r;
}

Personnage(float x, float y, Jetpack j) {
  this(x, y, 0f, 0f, j, 10f);
}
```
* Le premier constructeur nous sert à **instancier** un objet de la classe **Personnage** en spécifiant sa potision de départ, sa vitesse de départ, son jetpack et son rayon.
Le mot clé **super** fait référence à l'objet parent de l'instance courante. 
Ce qu'on fait dans le premier constructeur en utilisant le mot **super**, c'est qu'on appelle le constructeur de la classe PVector (la classe **parente**) pour définir la position initiale de notre Personnage (qui hérite de la classe PVector).

* Si on ne veut pas s'embêter à spécifier la vitesse de départ, on peut surcharger le premier constructeur (en créer un second avec des paramètres d'entrée différents) pour lequel on ne spécifie pas de vitesse.
Pour ne pas avoir à réécrire une deuxième fois un constructeur, on utilise le mot clé **this**.
En l'utilisant de cette manière, on appelle le premier constructeur de la classe **Personnage** (en spécifiant une vitesse nulle).

>:bulb:
>Cette fonctionnalité est très utile quand notre constructeur contient beaucoup de lignes de code car elle nous évite de tout réécrire.

<br>

### 4. Instanciation et utilisation

Pour ça, je te laisse instancier la variable *perso* de la classe Personnage en utilisant le constructeur de ton choix.
Puis il faut aussi que tu changes le code à l'intérieur de la fonction **draw()**.
Plus précisément, il faut changer ce qui est relatif aux *commandes*, à la vitesse, à la position, et au rayon du personnage.

Si tout marche bien, tant mieux.
Sinon voilà le code de l'onglet principal :
```java

Jetpack jet = new Jetpack(2f, 0.85f);
Environnement envrnt = new Environnement(new PVector(0f, -0.8f), 6e-6f);
Personnage perso = new Personnage(0f, 0f, jet);


void setup() {
  size(800, 600);  // taille de la fenêtre (en pixels)
  
  ellipseMode(RADIUS);
}

void draw() {
  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
  
  translate(width/2, height/2);
  scale(1, -1);
  
  perso.v.add(envrnt.g);
  
  if (perso.haut) {perso.v.add(jet.accHaut);}
  if (perso.bas) {perso.v.add(jet.accBas);}
  if (perso.gauche) {perso.v.add(jet.accGauche);}
  if (perso.droite) {perso.v.add(jet.accDroite);}
  
  float vMag = perso.v.mag();
  PVector trainee = perso.v.copy().normalize().mult(envrnt.rho * perso.r * perso.r * vMag * vMag).limit(vMag);
  perso.v.sub(trainee);
  perso.add(perso.v);
  ellipse(perso.x, perso.y, perso.r, perso.r);
  
  if(perso.y < -height / 2.) {perso.y += height;}
  if(perso.y > height / 2.) {perso.y -= height;}
  if(perso.x < -width / 2.) {perso.x += width;}
  if(perso.x > width / 2.) {perso.x -= width;}
}

void keyPressed() {
  commande(keyCode, true);
}

void keyReleased() {
  commande(keyCode, false);
}

void commande(int code, boolean activation) {
  switch (code) {
    case 37:
      perso.gauche = activation;
      break;
    case 38:
      perso.haut = activation;
      break;
    case 39:
      perso.droite = activation;
      break;
    case 40:
      perso.bas = activation;
      break;
  }
}
```

<br>

## Les méthodes

La fonction **commande** ne change que les variables *haut*, *bas*, *gauche*, et *droite*, qui sont des variables appartenant à la classe **Personnage**.
 De ce fait, on peut intégrer dans cette dernière la fonction *commande*.
 Munis-toi de ton plus beau **crtl+C / crtl+V** et déplace moi cette fonction dans la classe Personnage.
 
Il faut maintenant changer 4 fois la variable "**perso**" par le mot clé "**this**".
Par exemple :
```java
perso.gauche = activation;
```
devient :
```java
this.gauche = activation;
```
>:bulb:
>Pour rappel, "**this**" fait référence à l'instance courante de la classe.

<br>Enfin, dans les fonctions **keyPressed** et **keyReleased**, il faut spécifier qu'on utilise la fonction **commande** sur la variabl *perso*.
T'as surement compris comment faire, mais pour ceux du fond qui suivent pas :sleepy:, on fait comme ça (pour la fonction *keyPressed*) :
```java
perso.commande(keyCode, true);	
```

<br>

---
Si t'es arrivé jusqu'ici, déjà **bravo** :thumbsup: parce que c'est pas une mince affaire.
Et puis surtout, maintenant tu sais ce que c'est (en (très) gros) la **POO**, je t'en ai fais sentir la *philosophie*.

Ne te méprends pas, cette partie est **loin d'être exhaustive**, et tu as encore beaucoup à **apprendre** à propos de la **POO** (et moi aussi).
Que ce soit en termes d'**outils**, de **concepts** ou de **conventions**.

Pour faire de la POO, il faut savoir faire preuve d'**abstraction**.
C'est ce qui la rend **difficile** (pour moi compris), mais en même temps très **puissante**. 
Il faut savoir l'utiliser à **bon escient**. 
Par exemple, trop d'abstraction peut rendre un code **incompréhensible**, ce qui peut être problématique pour des projets de groupe.
Un peut de **simplicité** ne fait pas de mal parfois.

En attendant la suite, si tu veux :
*  En savoir plus sur la programmation en **java**, va regarder [ce tutoriel](https://openclassrooms.com/fr/courses/26832-apprenez-a-programmer-en-java/20304-installez-les-outils-de-developpement) (très bien fait, mais assez long) .
* Trouver de l'inspiration sur quoi programmer avec **processing**, va regarder [cette chaine youtube](https://www.youtube.com/c/TheCodingTrain/featured)
* Aller chercher directement l'information à la source, va voir les **doc** :
	* [ici (pour java)](https://docs.oracle.com/javase/7/docs/api/) 
	* et [ici (pour processing)](https://processing.org/reference/)

---
### Pour la suite

On va : 
* Implémenter des **collisions** (simplifiées) en utilisant les [Fonctions Distance Signées](https://fr.wikipedia.org/wiki/Fonction_distance_sign%C3%A9e) (SDF en anglais)
(il existe des librairies qui implémentent des moteurs physiques 2D tout prêts, mais j'aime bien me compliquer la vie :grin:)
* Implémenter un système de **score** et de **points de vie**
* Implémenter un **menu** et plusieurs **niveaux**

Pour cela, on verra :
* La **visibilité** en java (**scope** en anglais), et ce que ça implique.
* La **réécriture** de méthode, ou method **overriding**
(qui, avec la **surcharge**, sont 2 formes de **Polymorphisme**)
* Les **classes internes**, ou **inner class**
* Les **classes anonymes**
* Les **classes abstraites**
* Les **interfaces** et **interfaces fonctionnelles**
* Et quelques autres choses

<br>

By l'***Invocateur***
