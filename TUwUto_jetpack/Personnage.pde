class Personnage extends PVector {

  float r;
  
  PVector v;  //vitesse
  Jetpack jet;
  
  boolean gauche = false;
  boolean haut = false;
  boolean droite = false;
  boolean bas = false;
  
  Personnage(float x, float y, float vX, float vY, Jetpack j, float r) {
    super(x, y);
    this.v = new PVector(vX, vY);
    this.jet = j;
    this.r = r;
  }
  
  Personnage(float x, float y, Jetpack j) {
    this(x, y, 0f, 0f, j, 10f);
  }
  
  void commande(int code, boolean activation) {
    switch (code) {
      case 37:
        this.gauche = activation;
        break;
      case 38:
        this.haut = activation;
        break;
      case 39:
        this.droite = activation;
        break;
      case 40:
        this.bas = activation;
        break;
    }
  }
}
