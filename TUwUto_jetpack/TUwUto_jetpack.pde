
Jetpack jet = new Jetpack(2f, 0.85f);
Environnement envrnt = new Environnement(new PVector(0f, -0.8f), 6e-6f);
Personnage perso = new Personnage(0f, 0f, jet);

void setup() {
  size(800, 600);  // taille de la fenêtre (en pixels)
  
  ellipseMode(RADIUS);
}

void draw() {
  background(41);  // couleur du fond de la fenêtre en niveau de gris (0 = noir / 255 = blanc)
  
  translate(width/2, height/2);
  scale(1, -1);
  
  perso.v.add(envrnt.g);
  
  if (perso.haut) {perso.v.add(jet.accHaut);}
  if (perso.bas) {perso.v.add(jet.accBas);}
  if (perso.gauche) {perso.v.add(jet.accGauche);}
  if (perso.droite) {perso.v.add(jet.accDroite);}
  
  float vMag = perso.v.mag();
  PVector trainee = perso.v.copy().normalize().mult(envrnt.rho * perso.r * perso.r * vMag * vMag).limit(vMag);
  perso.v.sub(trainee);
  perso.add(perso.v);
  ellipse(perso.x, perso.y, perso.r, perso.r);
  
  if(perso.y < -height / 2.) {perso.y += height;}
  if(perso.y > height / 2.) {perso.y -= height;}
  if(perso.x < -width / 2.) {perso.x += width;}
  if(perso.x > width / 2.) {perso.x -= width;}
}

void keyPressed() {
  perso.commande(keyCode, true);
}

void keyReleased() {
  perso.commande(keyCode, false);
}
