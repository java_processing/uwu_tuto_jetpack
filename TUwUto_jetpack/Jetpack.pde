class Jetpack {
  
  PVector accHaut;
  PVector accBas;
  PVector accDroite;
  PVector accGauche;
  
  Jetpack(PVector aH, PVector aB, PVector aG, PVector aD) {
    this.accHaut = aH;
    this.accBas = aB;
    this.accGauche = aG;
    this.accDroite = aD;
  }
  
  Jetpack(float h, float b, float g, float d) {
    this(new PVector(0f, h),
         new PVector(0f, b),
         new PVector(g, 0f),
         new PVector(d, 0f));
  }
  
  Jetpack(float vertical, float lateral) {
    this(vertical, -vertical, -lateral, lateral);
  }
}
